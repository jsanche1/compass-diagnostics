from google.cloud import bigquery as bq
from google.cloud.exceptions import NotFound
from bs4 import BeautifulSoup
import requests
import pandas as pd
# import numpy as np
from datetime import datetime, timedelta
import re
from pandas_gbq.gbq import GenericGBQException
from multiprocessing import Pool
# from multiprocessing import cpu_count
import logging
import time


# service account file to authenticate with Google BigQuery
PRIVATE_KEY = './Analytics1-a4e570a06183.json'

# GBQ structure: project > dataset > tables
PROJECT_ID = 'expanded-nebula-754'
DATASET_ID = 'NIH_research_datasets'

# url of NIH ExPORTER catalog
NIH_EXPORTER_URL = 'https://exporter.nih.gov/ExPORTER_Catalog.aspx?index=0'


def _get_helper(ti):
    try:
        return CLIENT.get_table(ti.reference)
    except Exception:
        time.sleep(.01)
        return CLIENT.get_table(ti.reference)


def get_bq_tables():
    logging.info(f'accessing tables: {DATASET_REF.path}')
    with Pool(20) as p:
        tables = p.map(_get_helper, CLIENT.list_tables(DATASET_REF))

    bq_attrs = ['table_id',
                'created',
                'modified',
                'description',
                'num_rows',
                'num_bytes']

    tables = pd.DataFrame({k: [table.__getattribute__(k) for table in tables]
                           for k in bq_attrs})

    tables['modified'] = tables['modified'].apply(
            lambda t: t.replace(tzinfo=None))  # format to compare time w/ NIH

    logging.info(f'{tables.shape[0]} tables found.')
    return tables


def request_tab(url):
    headers = {"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6)\
    AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36"}
    r = requests.get(url, headers=headers)
    try:
        assert r.status_code == 200  # response OK
    except Exception as e:
        f = f"FAILURE: can't connect to url: {url}"
        logging.error(f'\n{f}\n{"_"*len(f)}\n\n', 'Error type: ', str(e), '\n',
                      type(e), '\n')
        logging.error(f'status: {r.status_code}')
        import sys
        sys.exit()
        # return -1
    # .content is encoded as binary (vs .text encoded as unicode)
    return r.content


def structure_html(html, label):
    soup = BeautifulSoup(html, 'html.parser')
    dfs = pd.read_html(str(soup.find_all('tr', {'class': 'row_bg'})[0].parent),
                       header=0)
    df = dfs[0]
    # BQ doesn't like spaces in header names
    df.columns = df.columns.map(lambda x: x.replace(' ', '_'))

    # parsing date columns
    df['Last_Updated_Date'] = df['Last_Updated_Date'].apply(
        lambda x: datetime.strptime(str(x), '%m/%d/%Y'))

    if label in ['patents', 'clinical studies']:
        df['Fiscal_Year'] = 'ALL'
    elif 'Fiscal_Year' not in df.columns:
        try:
            df['Fiscal_Year'] = df['Project_File_Name'].apply(
                lambda x: datetime.strptime(
                    re.search(r'(\d+)', str(x)).group(0), '%Y'))

        except Exception:
            df['Fiscal_Year'] = df['Last_Updated_Date']
    else:
        df['Fiscal_Year'] = df['Fiscal_Year'].apply(
            lambda x: datetime.strptime(str(x), '%Y'))

    # Pandas builtin doesn't scrape url of csv, so I add that col
    root = 'https://exporter.nih.gov/'
    df['csv_link'] = [root+soup.find_all('tr', {'class': 'row_bg'})[i]('td')
                      [df.columns.get_loc('CSV')].a.get('href')
                      for i in range(0, len(df['CSV']))]

    # consistent naming + wrapped up nicely for BQ table ranges
    df['tablename'] = df['Fiscal_Year']
    if 'Month' in df.columns:
        m = (df['Month'] != 'ALL')  # mask
        df.loc[m, 'tablename'] = df.loc[m, 'Month'].apply(
            lambda di: datetime.strptime(di.split(', WEEK ')[0], '%B %Y') +
            timedelta(days=7*(int(di.split(' WEEK ')[-1]) - 1))
        )
        df['entire_year'] = ~m
    else:
        df['entire_year'] = True
    if label not in ['patents', 'clinical studies']:
        df['tablename'] = df['tablename'].apply(lambda x: x.strftime('%Y%m%d'))
    label = label.upper().replace(' ', '_')  # include label in tablename
    year_marker = df.entire_year.map(lambda y: '' if y else 'WEEK_')
    df['tablename'] = 'NIH_' + label + '_' + year_marker + df['tablename']

    return df


def scrape_nih_tabs():
    logging.info('Starting NIH scraping...')
    # scrape NIH tabs
    tabs = ['projects',
            'abstracts',
            'publications',
            'patents',
            'clinical studies',
            'link tables']

    urls = [NIH_EXPORTER_URL[:-1] + str(i) for i in range(len(tabs))]
    scraped_tabs = [request_tab(url) for url in urls]
    logging.info('Structuring html... ')

    # structure tab HTML into DataFrame (defined above)
    structured_tabs = {label: structure_html(tab, label) for tab, label
                       in zip(scraped_tabs, tabs)}

    # join tabs together into one dataframe
    nih_cols = ['csv_link',
                'tablename',
                'entire_year',
                'Project_File_Name',
                'Last_Updated_Date']

    nih_all_tables = pd.concat([data[nih_cols].assign(tabname=tab) for tab,
                                data in structured_tabs.items()], axis=0)

    nih_all_tables = nih_all_tables.rename(columns={
        'Project_File_Name': 'description',
        'Last_Updated_Date': 'modified',
        'tablename': 'table_id'})

    logging.info(f'{nih_all_tables.shape[0]} NIH tables found.')
    return nih_all_tables


def nih_updates(nih, bq, update_all=False):
    '''Calculates updates to push to BQ'''
    merged = pd.merge(nih, bq, on='table_id', how='left', suffixes=('', '_bq'))
    if update_all:
        return merged
    f0 = merged.modified.astype(datetime) > merged.modified_bq.astype(datetime)
    f1 = (merged.modified_bq.isna())
    f2 = (merged.num_rows == 0) | (merged.num_bytes == 0)  # update if empty
    return merged.loc[f0 | f1 | f2, :]


def bq_udpates(nih, bq):
    f1 = ~bq.table_id.isin(nih.table_id)  # del everything not in NIH from BQ
    f2 = (bq.num_rows == 0) | (bq.num_bytes == 0)  # remove empty tables
    return bq[f1 | f2]


def table_exists(table_id, dataset_id=DATASET_ID):
    table_ref = CLIENT.dataset(dataset_id).table(table_id)
    try:
        CLIENT.get_table(table_ref)
        return True
    except NotFound as e:
        return False


def table_delete(table_id, dataset_id=DATASET_ID):
    table_ref = CLIENT.dataset(dataset_id).table(table_id)
    CLIENT.delete_table(table_ref)


def upload_nih_table(row):
    project_id = PROJECT_ID
    dataset_id = DATASET_ID
    private_key = PRIVATE_KEY

    table_id = row.table_id
    desc = row.description
    link = row.csv_link
    destination_table = dataset_id + '.' + table_id
    try:
        # pandas can read csv from a url + unzip the file
        logging.info(f'reading file for {table_id}: {link.split("/")[-1]}...')
        data = pd.read_csv(link, compression='zip', quotechar='"',
                           encoding="ISO-8859-1", dtype=str)

        data = data.rename(columns=lambda x: re.sub(r'\W', '_', x))  # BQ cols
        data = data.applymap(lambda s: str(s).replace('\n', ' '))  # quoted \n
        n_rows = data.shape[0]
        logging.info(f'read {table_id} ({n_rows} rows, \
                     {data.shape[1]} cols)')

        logging.info(f'pushing table {table_id} into BigQuery... ')
        try:
            data.to_gbq(destination_table, project_id, if_exists='replace',
                        private_key=private_key)
            logging.info(f'table {table_id} finished loading')
        except GenericGBQException as e:
            logging.error(f'[ERROR] generic BQ exception while uploading table \
                          {table_id}...')

            logging.error(str(e))
            return (table_id, 'Generic Exception')

        table_ref = CLIENT.dataset(dataset_id).table(table_id)
        try:
            # verify upload has same number of rows
            table = CLIENT.get_table(table_ref)
            logging.info(f'table {table_id} has {table.num_rows} rows \
                         ({table.num_bytes} bytes)')
            try:
                assert n_rows == table.num_rows, f'[WARNING] table \
                                {table_id} should have {n_rows} rows'

            except AssertionError as e:
                logging.warning(e)
                return (table_id, 'Mismatched row count')

            # update table description
            table.description = desc
            table = CLIENT.update_table(table, ['description'])
            try:  # verify description
                s = f'[WARNING] table {table_id} description failed to update'
                assert table.description == desc, s
            except AssertionError as e:
                logging.warning(e)
                return (table_id, 'Description failed to update')

        except NotFound as e:
            logging.error(f'Table {table_id} was not found.')
            return (table_id, 'Not Found')

    except Exception as e:
        logging.error(f'[ERROR] uncaught Exception for table {table_id}')
        logging.error(str(e))
        return (table_id, 'Uncaught Exception')

    return (table_id, 0)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-q', '--quiet', action="store_true")
    parser.add_argument('--update_all', action="store_true", )
    arg = parser.parse_args()

    logging.basicConfig(format='%(asctime)s [%(levelname)s]: %(message)s',
                        level=logging.WARNING if arg.quiet else logging.INFO)

    t0 = time.time()
    # Connect to BigQuery
    CLIENT = bq.Client.from_service_account_json(PRIVATE_KEY)
    DATASET_REF = CLIENT.dataset(DATASET_ID)
    bq_tables = get_bq_tables()  # BQ tables
    nih_tables = scrape_nih_tabs()  # NIH tables (combined tabs)
    update_all = True if arg.update_all else False
    nih_updates_all = nih_updates(nih_tables, bq_tables, update_all=update_all)

    # TODO handle scheduled BQ removals

    updates = list(map(lambda i: i[1], nih_updates_all.iterrows()))
    logging.info(f'{len(updates)} tables to update')

    with Pool(10) as p:
        ret = p.map(upload_nih_table, updates)

    failed = list(filter(lambda t: t[1] != 0, ret))
    for f in failed:
        logging.warning(f'[FAILED]<{f}>')
    logging.info(f'TIME: took {time.time()-t0:.1f} s')

    import sys
    sys.exit()
